/////////////////////////////////////////////////
// raw2bbc2 - A small utility to convert raw
//            8-bit indexed images to Acorn's
//            BBC Micro Mode 2 format.
//
//            Image size should be multiple of
//            4x8. In case it's not, it will be
//            padded.
//
//            Every byte contains 4 pixels stored
//            as interleaved high and low bits
//            H0H1H2H3L0L1L2L3
//
// By Roberto Carlos Fernández Gerhardt (robcfg)
/////////////////////////////////////////////////
#define _CRT_SECURE_NO_WARNINGS

#include <cstdio>
#include <cstdlib>
#include <string>
#include <iostream>

using namespace std;

void ShowUsage()
{
    cout << "raw2bbc2 usage:" << endl << endl;
    cout << "     raw2bbc2 input_file_name width height" << endl;
    cout << "     i.e. raw2bbc2 screen.raw 320 200" << endl << endl;
}

// Load image from file, making sure it fits the buffer padding.
bool LoadImage( const string& _filename,
                unsigned long int _imageWidth,
                unsigned long int _imageHeight,
                unsigned long int _bufferWidth,
                unsigned long int _bufferHeight,
                unsigned char* _dest
                )
{
    FILE* inFile = fopen( _filename.c_str(), "rb" );
    if( NULL == inFile )
    {
        return false;
    }

    size_t destOffset = 0;
    for( unsigned long int line = 0; line < _bufferHeight; ++line )
    {
        destOffset = line * _bufferWidth;
        fread( _dest+destOffset, 1, _imageWidth, inFile );
    }

    fclose( inFile );

    return true;
}

unsigned char PackPixels( const unsigned char* _buffer )
{
    unsigned char retVal = 0;

    for( unsigned char pixel = 0; pixel < 4; ++pixel )
    {
        retVal |= ((_buffer[pixel] & 1) ? 1:0) << (3-pixel);
        retVal |= ((_buffer[pixel] & 2) ? 1:0) << (7-pixel);
    }

    return retVal;
}

void PackTiles( const unsigned char* _buffer, 
                unsigned char* _destBuffer, 
                unsigned long int _imageWidth,
                unsigned long int _imageHeight,
                unsigned long int _bufferWidth, 
                unsigned long int _bufferHeight )
{
    size_t srcOffset = 0;
    size_t dstOffset = 0;

    for( int y = 0; y < _bufferHeight; y += 8 )
    {
        for( int x = 0; x < _bufferWidth; x += 4 )
        {            
            for( unsigned char line = 0; line < 8; ++line )
            {
                srcOffset = ((y+line) * _imageWidth) + x;

                _destBuffer[dstOffset++] = PackPixels( &_buffer[srcOffset] );
            }
        }
    }
}

int main( int argc, char** argv )
{
    // Check parameters
    if( argc < 4 ) // program name, file name, width and height
    {
        ShowUsage();
        return -1;
    }

    // Pad image size if needed and alloc buffer
    auto imageWidth   = strtoul(argv[2], NULL, 0);
    auto imageHeight  = strtoul(argv[3], NULL, 0);
    auto bufferWidth  = imageWidth  % 4 ? (imageWidth +(4-(imageWidth  % 4))):imageWidth;
    auto bufferHeight = imageHeight % 8 ? (imageHeight+(8-(imageHeight % 8))):imageHeight;
    auto bufferSize   = bufferWidth * bufferHeight;

    auto buffer = new unsigned char[bufferSize];
    if( NULL == buffer )
    {
        cout << "[Error] Couldn't allocate " << bufferSize << " bytes of memory for loading image." << endl;
        return -1;
    }
    memset( buffer, 0, bufferSize );

    // Load image into buffer
    if( !LoadImage( argv[1], imageWidth, imageHeight, bufferWidth, bufferHeight, buffer) )
    {
        cout << "[Error] Couldn't load file " << argv[1] << endl;
        delete[] buffer;
        return -1;
    }

    // Alloc destination buffer
    auto destBuffer = new unsigned char[bufferSize / 4];

    // Pack 4x8 sized tiles to 8 contiguous bytes
    PackTiles( buffer, destBuffer, imageWidth, imageHeight, bufferWidth, bufferHeight );

    // Generate destination file name and save
    string dstFilename = argv[1];
    dstFilename += ".bin";

    FILE* outFile = fopen( dstFilename.c_str(), "wb" );
    if( NULL == outFile )
    {
        cout << "[Error] Couldn't create/overwrite destination file ";
        cout << dstFilename.c_str() << endl;
        delete[] destBuffer;
        delete[] buffer;
        return -1;
    }

    size_t dstFileSize = bufferWidth * imageHeight / 4;
    fwrite( destBuffer, 1, dstFileSize, outFile );
    fclose( outFile );

    // Clean up
    delete[] destBuffer;
    delete[] buffer;

    return 0;
}